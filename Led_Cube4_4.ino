#include <SPI.h>
#include "GyverButton.h"

#define BUTTON1 A4
#define BUTTON2 A5
#define RED_LED A0
#define GREEN_LED A1

GButton butt1(BUTTON1);
GButton butt2(BUTTON2);

#define POS_X 0
#define NEG_X 1
#define POS_Z 2
#define NEG_Z 3
#define POS_Y 4
#define NEG_Y 5

#define XAXIS 0
#define YAXIS 1
#define ZAXIS 2

#define INVERT_Y 1   // инвертировать по вертикали (если дождь идёт вверх)
#define INVERT_X 0    // инвертировать по горизонтали (если текст не читается)

#define RAIN_TIME 1600
#define PLANE_BOING_TIME 1500
#define SEND_VOXELS_TIME 640
#define WOOP_WOOP_TIME 50
#define CUBE_JUMP_TIME 1500
#define GLOW_TIME 1600
#define TEXT_TIME 300
#define CLOCK_TIME 500
#define WALKING_TIME 100

#define TOTAL_EFFECTS 10  // количество эффектов

#define RAIN 0
#define PLANE_BOING 1
#define SEND_VOXELS 2
#define WOOP_WOOP 3
#define CUBE_JUMP 4
#define GLOW 5
#define TEXT 6
#define LIT 7


unsigned long currentTime = 0; //для организации циклов прерывания
unsigned long cicle_update = 60000; //основной цикл минуты

uint8_t cube[4][4];
int8_t currentEffect;
uint16_t timer;
uint16_t modeTimer;
bool loading;
int8_t pos;
int8_t vector[3];
int16_t coord[3];
bool debug = true;

void setup() {
  Serial.begin(9600);

  loading = true;

  SPI.begin();
  SPI.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));

  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  randomSeed(analogRead(0));
  digitalWrite(GREEN_LED, HIGH);
  digitalWrite(RED_LED, HIGH);

  butt1.setIncrStep(5);         // настройка инкремента, может быть отрицательным (по умолчанию 1)
  butt1.setIncrTimeout(100);    // настрйока интервала инкремента (по умолчанию 800 мс)
  butt2.setIncrStep(-5);        // настройка инкремента, может быть отрицательным (по умолчанию 1)
  butt2.setIncrTimeout(100);    // настрйока интервала инкремента (по умолчанию 800 мс)

  currentEffect = RAIN;
  changeMode();
  currentTime = millis();

  if (debug)Serial.println("Setup end");
}

void loop() {
  butt1.tick();
  butt2.tick();

  //основной цикл 3сек
  if (millis() > (currentTime + cicle_update)) {
    currentTime = millis();
    incrMode();
  } else if (currentTime > millis()) {
    currentTime = millis();
    if (debug)Serial.println("Overflov cicle_update");
  }

  if (butt1.isSingle()) {
    incrMode();
  }
  if (butt2.isSingle()) {
    currentEffect--;
    if (currentEffect < 0) currentEffect = TOTAL_EFFECTS - 1;
    changeMode();
  }

  if (butt1.isIncr()) {                                 // если кнопка была удержана (это для инкремента)
    modeTimer = butt1.getIncr(modeTimer);               // увеличивать/уменьшать переменную value с шагом и интервалом
  }
  if (butt2.isIncr()) {                                 // если кнопка была удержана (это для инкремента)
    modeTimer = butt2.getIncr(modeTimer);               // увеличивать/уменьшать переменную value с шагом и интервалом
  }

  switch (currentEffect) {
    case RAIN: rain(); break;
    case PLANE_BOING: planeBoing(); break;
    case SEND_VOXELS: sendVoxels(); break;
    //case WOOP_WOOP: woopWoop(); break;
    case CUBE_JUMP: cubeJump(); break;
    case GLOW: glow(); break;
    case LIT: lit(); break;
    case 8: sinusFill(); break;
    case 9: sinusThin(); break;
    case 10: walkingCube(); break;
    
    default: rain(); break;
  }
  renderCube();
}
