void incrMode() {
  currentEffect++;
  if (currentEffect >= TOTAL_EFFECTS) currentEffect = 0;
  changeMode();
}

void changeMode() {
  clearCube();
  loading = true;
  timer = 0;
  randomSeed(millis());
  digitalWrite(RED_LED, HIGH);
  digitalWrite(GREEN_LED, LOW);
  delay(500);
  digitalWrite(RED_LED, LOW);
  digitalWrite(GREEN_LED, HIGH);

  switch (currentEffect) {
    case RAIN: modeTimer = RAIN_TIME; break;
    case PLANE_BOING: modeTimer = PLANE_BOING_TIME; break;
    case SEND_VOXELS: modeTimer = SEND_VOXELS_TIME; break;
    case WOOP_WOOP: modeTimer = WOOP_WOOP_TIME; break;
    case CUBE_JUMP: modeTimer = CUBE_JUMP_TIME; break;
    case GLOW: modeTimer = GLOW_TIME; break;
    case TEXT: modeTimer = TEXT_TIME; break;
    case LIT: modeTimer = CLOCK_TIME; break;
    case 8: modeTimer = RAIN_TIME; break;
    case 9: modeTimer = RAIN_TIME; break;
    case 10: modeTimer = WALKING_TIME; break;
  }
  // modeTimer = RAIN_TIME;
}

void clearCube() {
  for (uint8_t i = 0; i < 4; i++) {
    for (uint8_t j = 0; j < 4; j++) {
      cube[i][j] = 0;
    }
  }
}

void lightCube() {
  for (uint8_t i = 0; i < 4; i++) {
    for (uint8_t j = 0; j < 4; j++) {
      cube[i][j] = 0xFF;
    }
  }
}

void renderCube() {
  uint8_t tmpcube;
  for (uint8_t i = 0; i < 4; i++) {
    digitalWrite(SS, LOW);
    if (INVERT_Y) SPI.transfer(0x01 << (3 - i));
    else SPI.transfer(0x01 << i);
    for (uint8_t j = 1; j < 3; j++) {
      if (INVERT_X) {
        if (j == 1) {
          tmpcube = cube[i][j + 2] << 4;
          tmpcube |= cube[i][j + 1];
        } else {
          tmpcube = cube[i][j - 1] << 4 ;
          tmpcube |=  cube[i][j - 2];
        }
        SPI.transfer(tmpcube);
      }
      else {
        if (j == 1) {
          // if (debug)Serial.print(cube[i][j - 1]);
          //if (debug)Serial.print("");
          //if (debug)Serial.print(cube[i][j]);
          // if (debug)Serial.print("");
          tmpcube = cube[i][j - 1] << 4 ;
          tmpcube |=  cube[i][j];
          // if (debug)Serial.println(tmpcube);
        } else {
          // if (debug)Serial.print(cube[i][j]);
          // if (debug)Serial.print("");
          //  if (debug)Serial.print(cube[i][j + 1]);
          // if (debug)Serial.print("");
          tmpcube = cube[i][j] << 4;
          tmpcube |= cube[i][j + 1];
          // if (debug)Serial.println(tmpcube);
        }
        SPI.transfer(tmpcube);
      }
      // Serial.println(cube[i][j]);
    }
    digitalWrite(SS, HIGH);
    //delay(1);
  }
}

void shift(uint8_t dir) {

  if (dir == POS_X) {
    for (uint8_t y = 0; y < 4; y++) {
      for (uint8_t z = 0; z < 4; z++) {
        cube[y][z] = cube[y][z] << 1;
      }
    }
  } else if (dir == NEG_X) {
    for (uint8_t y = 0; y < 4; y++) {
      for (uint8_t z = 0; z < 4; z++) {
        cube[y][z] = cube[y][z] >> 1;
      }
    }
  } else if (dir == POS_Y) {
    for (uint8_t y = 1; y < 4; y++) {
      for (uint8_t z = 0; z < 4; z++) {
        cube[y - 1][z] = cube[y][z];
      }
    }
    for (uint8_t i = 0; i < 4; i++) {
      cube[3][i] = 0;
    }
  } else if (dir == NEG_Y) {
    for (uint8_t y = 3; y > 0; y--) {
      for (uint8_t z = 0; z < 4; z++) {
        cube[y][z] = cube[y - 1][z];
      }
    }
    for (uint8_t i = 0; i < 4; i++) {
      cube[0][i] = 0;
    }
  } else if (dir == POS_Z) {
    for (uint8_t y = 0; y < 4; y++) {
      for (uint8_t z = 1; z < 4; z++) {
        cube[y][z - 1] = cube[y][z];
      }
    }
    for (uint8_t i = 0; i < 4; i++) {
      cube[i][3] = 0;
    }
  } else if (dir == NEG_Z) {
    for (uint8_t y = 0; y < 4; y++) {
      for (uint8_t z = 3; z > 0; z--) {
        cube[y][z] = cube[y][z - 1];
      }
    }
    for (uint8_t i = 0; i < 4; i++) {
      cube[i][0] = 0;
    }
  }
}

void setVoxel(uint8_t x, uint8_t y, uint8_t z) {
  cube[3 - y][3 - z] |= (0x01 << x);
}

void clearVoxel(uint8_t x, uint8_t y, uint8_t z) {
  cube[3 - y][3 - z] ^= (0x01 << x);
}

bool getVoxel(uint8_t x, uint8_t y, uint8_t z) {
  return (cube[3 - y][3 - z] & (0x01 << x)) == (0x01 << x);
}

void drawCube(uint8_t x, uint8_t y, uint8_t z, uint8_t s) {
  if (debug)Serial.print("drawCube ");
  if (debug)Serial.print(x);
  if (debug)Serial.print(y);
  if (debug)Serial.print(z);
  if (debug)Serial.println(s);
  for (uint8_t i = 0; i < s; i++) {
    setVoxel(x, y + i, z);
    setVoxel(x + i, y, z);
    setVoxel(x, y, z + i);
    setVoxel(x + s - 1, y + i, z + s - 1);
    setVoxel(x + i, y + s - 1, z + s - 1);
    setVoxel(x + s - 1, y + s - 1, z + i);
    setVoxel(x + s - 1, y + i, z);
    setVoxel(x, y + i, z + s - 1);
    setVoxel(x + i, y + s - 1, z);
    setVoxel(x + i, y, z + s - 1);
    setVoxel(x + s - 1, y, z + i);
    setVoxel(x, y + s - 1, z + i);
  }
}

void print_Cube() {
  for (uint8_t y = 0; y < 4; y++) {
    for (uint8_t z = 0; z < 4; z++) {
      Serial.print(cube[y][z]);
      Serial.print(" ");
    }
    Serial.println();
  }
  Serial.println();
}
